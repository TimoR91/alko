<?php

// settings
$filename = "data/alkon-hinnasto.csv";
$filename_original = "data/alkon-hinnasto-ascii_original.csv";
$filename_xlxs = "alko.xlsx";
$priceListDate = "14.09.2020";

// define columns to include in the display
$columns2Include = [
    "Numero",
    "Nimi",
    "Valmistaja",
    "Pullokoko",
    "Hinta",
    "Litrahinta",
    /*    
"Uutuus",
"Hinnastojärjestyskoodi",
 */
    "Tyyppi",
    /*
"Alatyyppi",
"Erityisryhmä",
"Oluttyyppi",
 */
    "Valmistusmaa",
    /*
"Alue",
 */
    "Vuosikerta",
    /*
"Etikettimerkintöjä",
"Huomautus",
"Rypäleet",
"Luonnehdinta",
"Pakkaustyyppi",
"Suljentatyyppi",
 */
    "Alkoholi-%",
    /*
"Hapot g/l",
"Sokeri g/l-%",
"Väri EBC",
"Katkerot EBU",
 */
    "Energia kcal/100 ml",
    /*
"Valikoima",
"EA"
 * 
 */
];


/* all columns listed below
 * Numero;
 * Nimi;
 * Valmistaja;
 * Pullokoko;
 * Hinta;
 * Litrahinta;
 * Uutuus;
 * Hinnastojärjestyskoodi;
 * Tyyppi;
 * Alatyyppi;
 * Erityisryhmä;
 * Oluttyyppi;
 * Valmistusmaa;
 * Alue;
 * Vuosikerta;
 * Etikettimerkintöjä;
 * Huomautus;
 * Rypäleet;
 * Luonnehdinta;
 * Pakkaustyyppi;
 * Suljentatyyppi;
 * Alkoholi-%;
 * Hapot g/l;
 * Sokeri g/l;
 * Kantavierrep-%;
 * Väri EBC;
 * Katkerot EBU;
 * Energia kcal/100 ml;
 * Valikoima;
 * EAN
 */
