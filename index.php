<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Alkon hinnasto</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <?php
    require "model.php";
    $filters = handleRequest();
    $rowsPage = $filters['LIMIT'];
    $pageNumber = $filters['PAGE'];
    $nextPage = $pageNumber + 1;
    $prevPage = $pageNumber - 1;
    $currentPage = $_GET['page'];
    $typeInput = $filters['TYPE'];
    $current_uri = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $current_url = strtok($current_uri, '?');

    $alkoData = initModel($filters);

    $filteredData = applyFilters($alkoData, $filters);
    $alkoProductTable = generateView($filteredData, $filters);
    $rowsFound = count($filteredData);
    $isLastPage = $pageNumber >= floor(($rowsFound - 1) / $rowsPage);

    echo "<div class=\"tbl-header alert alert-success\" role=\"\">Alkon hinnasto $priceListDate (Total items $rowsFound)</div>";

    // Filters form
    $start = $current_url . "?page=0";
    echo generateFilters($alkoData, $filters, $start);

    // display products table here
    echo $alkoProductTable;

    $next = $current_url . "?page=" . $nextPage;
    $prev = $current_url . "?page=" . $prevPage;

    // filters to add (to the end of url)
    $filters_to_add = "";

    foreach ($filters as $key => $value) {
        if ($value !== null && $key !== "PAGE" && $key !== "MODE") {
            $filters_to_add .= "&" . strtolower($key) . "=" . $value;
        }
    }

    $disabledPrevPage = $currentPage < 1 ? 'disabled=\"\"' : '';
    $prevButtonClass = $currentPage < 1 ? "btn btn-secondary" : "btn btn-primary";

    $disabledNextPage = $isLastPage ? 'disabled=\"\"' : '';
    $nextButtonClass = $isLastPage ? "btn btn-secondary" : "btn btn-primary";

    ?>
    <div class="footer">
        <?php
        echo "<a href=\"$prev$filters_to_add\" class=\"$prevButtonClass\" type=\"button\" $disabledPrevPage>Previous page</a>";
        echo "<a href=\"$start$filters_to_add\" class=\"btn btn-success\" type=\"button\">Start page</a>";
        echo "<a href=\"$next$filters_to_add\" class=\"$nextButtonClass\" type=\"button\" $disabledNextPage>Next page</a>";
        echo "<a href=\"$start$filters_to_add&mode=update\" class=\"btn btn-warning\" type=\"button\">Update CSV</a>";
        ?>
    </div>

</body>

</html>