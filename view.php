<?php

function sortBySize($a, $b)
{
    return floatval(str_replace(',', '.', $a)) > floatval(str_replace(',', '.', $b));
}

function generateValueOptions($products, $column, $selectedValue, $sortFunc = null)
{
    $values = array_filter(array_unique(array_map(
        function ($product) use ($column) {
            global $columnNamesMap;
            return $product[$columnNamesMap[$column]];
        },
        $products
    )));

    if ($sortFunc) {
        usort($values, $sortFunc);
    } else {
        sort($values);
    }

    return array_reduce($values, function ($options, $value) use ($selectedValue) {
        $selected = $value == $selectedValue ? 'selected' : '';
        return $options . "<option $selected>$value</option>\n";
    }, '');
}

function generateFilters($products, $filters, $start)
{
    $filtersForm = "
             <div class='tbl-filters'>
               <form>
            ";

    $typeOptions = generateValueOptions($products, 'Tyyppi', $filters['TYPE']);
    $defaultTypeSelected = !$filters['TYPE'] ? 'selected' : '';
    $filtersForm .= "
                    <label for='type-input'></label><br>
                    <select class='form-control mb-2' id='type-input' name='type'>
                        <option value='' disabled {$defaultTypeSelected} hidden>Tyyppi</option>
                        {$typeOptions}
                    </select>";

    $countryOptions = generateValueOptions($products, 'Valmistusmaa', $filters['COUNTRY']);
    $defaultCountrySelected = !$filters['COUNTRY'] ? 'selected' : '';
    $filtersForm .= "
                    <label for='country-input'></label><br>
                    <select class='form-control mb-2' id='country-input' name='country'>
                        <option value='' disabled {$defaultCountrySelected} hidden>Valmistusmaa</option>
                        {$countryOptions}
                    </select>";

    $sizeOptions = generateValueOptions($products, 'Pullokoko', $filters['SIZE'], 'sortBySize');
    $defaultSizeSelected = !$filters['SIZE'] ? 'selected' : '';
    $filtersForm .= "
                    <label for='size-input' ></label><br>
                    <select class='form-control mb-2' id='size-input' name='size'>
                        <option value='' disabled {$defaultSizeSelected} hidden>Pullokoko</option>
                        {$sizeOptions}
                    </select>
                ";
    $priceLowFilter = $filters['PRICELOW'];
    $filtersForm .= "
                    <label for='pricelow-input'></label><br>
                    <input class='form-control mb-2' type='number' id='pricelow-input' value='$priceLowFilter' name='pricelow' placeholder='Hinta-ala'>
                ";
    $priceHighFilter = $filters['PRICEHIGH'];
    $filtersForm .= "
                    <label for='priceHigh-input'></label><br>
                    <input class='form-control mb-2' type='number' id='pricehigh-input' value='$priceHighFilter' name='pricehigh' placeholder='Hinta-ylä'>
                ";
    $energyLowFilter = $filters['ENERGYLOW'];
    $filtersForm .= "
                    <label for='energyLow-input'></label><br>
                    <input class='form-control mb-2' type='number' id='energylow-input' value='$energyLowFilter' name='energylow' placeholder='Energia-ala'>
                ";
    $energyHighFilter = $filters['ENERGYHIGH'];
    $filtersForm .= "
                    <label for='energyhigh-input'></label><br>
                    <input class='form-control mb-2' type='number' id='energyhigh-input' value='$energyHighFilter' name='energyhigh' placeholder='Energia-ylä'>
                    <input class='btn btn-primary mb-2' type='submit' value='Filter' />
                    <a href='$start' class='btn btn-primary mb-2' type='button'>Reset</a>
                </form>
        </div>";

    return $filtersForm;
}

function createColumnHeaders($columns2Include)
{
    $t = "<thead>\n";
    $t .= "<tr>\n";
    for ($i = 0; $i < count($columns2Include); $i++) {
        $val = $columns2Include[$i];
        $t .= '<th scope="col">' . $val . "</th>\n";
    }
    $t .= "</tr>\n</thead>\n";
    return $t;
}

function createTableRow($product, $columns2Include, $columnNamesMap)
{
    $t = "<tr>\n";
    for ($i = 0; $i < count($columns2Include); $i++) {
        $columnName = $columns2Include[$i];
        $item = $product[$columnNamesMap[$columnName]];
        if ($i == 0) {
            $t .= '<th scope="row">' . $item . "</th>";
        } else {
            $t .= "<td>" . $item . "</td>";
        }
    }
    $t .= "\n</tr>\n";
    return $t;
}

/**
 * Creates a html-table from alko products
 * @param array $products array of products
 * @param type $columns2Include names of columns to include
 * @param type $columnNamesMap column names to index mas
 * @param $filters['LIMIT'] max nbr of items to include
 *        $filters['PAGE'] page to start from
 *        $filters['TYPE'] product type
 *        $filters['COUNTRY'] product country
 *        $filters['PRICELOW'] price low limit
 *        $filters['PRICEHIGH'] price high limit
 * @return string html table
 */

function createAlkoProductsTable($products, $columns2Include, $columnNamesMap, $filters)
{
    $limitCounter = 0;
    $limitCounterLow = $filters['LIMIT'] * $filters['PAGE'];
    $limitCounterHigh = $limitCounterLow + $filters['LIMIT'];

    $t = '<table id="products" class="table">';
    $t .= createColumnHeaders($columns2Include);
    $t .= '<tbody>';
    for ($i = 0; $i < count($products); $i++) {
        $product = $products[$i];

        // limit items to include into table
        $limitCounter++;
        if ($limitCounter > $limitCounterLow) {
            $t .= createTableRow($product, $columns2Include, $columnNamesMap);
            if ($limitCounter >= $limitCounterHigh) {
                break;
            }
        }
    }
    $t .= '</tbody>';
    $t .= "</table>";
    return $t;
}

function applyFilters($alkoData, $filters)
{
    $filtered = array_filter($alkoData, function ($product) use ($filters) {
        global $columnNamesMap;
        return ($filters['TYPE'] == null || ($product[$columnNamesMap['Tyyppi']] == $filters['TYPE'])) &&
            ($filters['COUNTRY'] == null || ($product[$columnNamesMap['Valmistusmaa']] == $filters['COUNTRY'])) &&
            ($filters['PRICELOW'] == null || ($product[$columnNamesMap['Hinta']] >= $filters['PRICELOW'])) &&
            ($filters['PRICEHIGH'] == null || ($product[$columnNamesMap['Hinta']] <= $filters['PRICEHIGH'])) &&
            ($filters['ENERGYLOW'] == null || ($product[$columnNamesMap['Energia kcal/100 ml']] >= $filters['ENERGYLOW'])) &&
            ($filters['ENERGYHIGH'] == null || ($product[$columnNamesMap['Energia kcal/100 ml']] <= $filters['ENERGYHIGH'])) &&
            ($filters['SIZE'] == null || ($product[$columnNamesMap['Pullokoko']] == $filters['SIZE']));
    });
    return array_values($filtered);
}

function generateView($alkoData, $filters)
{
    global $columns2Include, $columnNamesMap;

    if ($filters['MODE'] === 'view' || $filters['MODE'] === 'update') {
        $alkoProductTable = createAlkoProductsTable(
            $alkoData,
            $columns2Include,
            $columnNamesMap,
            $filters
        );
        return $alkoProductTable;
    } else {

        return "<h2>Unknown command </h2>";
    }
}