<?php

// url parameters must have correct values if not defined in url

function handleRequest()
{
    $filters['MODE'] = $_GET['mode'] ?? 'view';
    $filters['TYPE'] = $_GET['type'] ?? null;
    $filters['LIMIT'] = $_GET['limit'] ?? 25;
    $filters['PAGE'] = $_GET['page'] ?? 0;
    $filters['COUNTRY'] = $_GET['country'] ?? null;
    $filters['PRICELOW'] = $_GET['pricelow'] ?? null;
    $filters['PRICEHIGH'] = $_GET['pricehigh'] ?? null;
    $filters['ENERGYHIGH'] = $_GET['energyhigh'] ?? null;
    $filters['ENERGYLOW'] = $_GET['energylow'] ?? null;
    $filters['SIZE'] = $_GET['size'] ?? null;

    return $filters;
}
