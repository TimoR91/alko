<?php

require_once("config.php");
require_once("view.php");
require_once("controller.php");

$cs = ini_get("default_charset");
ini_set("auto_detect_line_endings", true);

$columnNames = [];
$columnNamesMap = [];
$alkoData = [];

function readPriceList($filename)
{
    // return values as global data
    global $priceListDate, $columnNames, $filename, $filename_original;

    $row = 0;
    $alkoDataIndex = 0;
    $alkoData = [];

    // try to open latest csv
    if (file_exists($filename))

        $handle = fopen($filename, "r");

    else if (file_exists($filename_original)) {

        $handle = fopen($filename_original, "r");
    } else {
        echo '<h2>No csv file found</h2>';
        return;
    }

    // if file was found, create alko data
    if ($handle !== FALSE) {

        $locking_tries = 5;

        // lock mutex
        while (flock($handle, LOCK_SH) === false && $locking_tries >= 0) {

            $locking_tries--;
            sleep(0.1);
        }

        // mutex lock failed
        if ($locking_tries <= 0) {
            fclose($handle);
            echo '<h2>Mutex lock failed while reading price list</h2>';
            return;
        }

        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            if ($row === 0) {
                // Alkon hinnasto xx.xx.xxxx                
                $key = "Alkon hinnasto ";
                $keyLen = strlen($key);

                if ($key == substr($data[0], 0, strlen($key))) {
                    $priceListDate = substr($data[0], strlen($key));
                }
            } else if ($row === 1) {
                    // Huom! ...- skipping
                ;
            } else if ($row === 2) {
                    // empty line - skipping
                ;
            } else if ($row === 3) {
                // header names
                $columnNames = $data;
            } else {
                // normal rows starts here
                $alkoData[$alkoDataIndex] = $data;
                $alkoDataIndex++;
            }
            $row++;
        }

        // unlock mutex
        flock($handle, LOCK_UN);

        fclose($handle);
    }
    return $alkoData;
}

function createColumnNamesMap($cn)
{
    $cnMap = [];
    for ($i = 0; $i < count($cn); $i++) {
        $cnMap[$cn[$i]] = $i;
    }
    return $cnMap;
}

function initModel($filters)
{
    global $alkoData, $columnNames, $columnNamesMap, $filename;

    if ($filters['MODE'] === 'update') {

        updateCSV();
    }

    $alkoData = readPriceList($filename);
    $columnNamesMap = createColumnNamesMap($columnNames);

    return $alkoData;
}

$remote_filename_xlsl = "https://www.alko.fi/INTERSHOP/static/WFS/Alko-OnlineShop-Site/-/Alko-OnlineShop/fi_FI/Alkon%20Hinnasto%20Tekstitiedostona/alkon-hinnasto-tekstitiedostona.xlsx";

require_once(__DIR__ . '/vendor/shuchkin/simplexlsx/src/SimpleXLSX.php');
function updateCSV()
{
    global $filename, $remote_filename_xlsl;

    // try to parse xlsx data from an url
    $xlsx_data = SimpleXLSX::parseData(file_get_contents($remote_filename_xlsl));

    // if parsing succeeded
    if ($xlsx_data !== false) {

        // attempt to create a temporary csv file
        $csv_memorylimit = 5 * 1024 * 1024;
        $temp_handle = fopen("php://temp/maxmemory:$csv_memorylimit", "w");

        // if failed to create a temporary csv file, return
        if ($temp_handle === false) {
            echo '<h2>Error: couldnt create/open temporary file for csv update</h2>';
            return;
        }

        $csv_write_succeeded = true;
        $xlsx_rows = $xlsx_data->rows();

        foreach ($xlsx_rows as $row) {

            // write xlsx data to the temporary csv file
            if (fputcsv($temp_handle, $row, ";") === false) {

                // writing to temp csv failed
                $csv_write_succeeded = false;
                break;
            }
        }

        // if write to temp csv succeeded, write to the actual csv
        if ($csv_write_succeeded === true) {

            rewind($temp_handle);
            $latest_csv_data = stream_get_contents($temp_handle);

            $handle = fopen($filename, "w");

            if ($handle === false) {
                echo "<h2>Error: couldnt open the csv file handle $filename</h2>";
            } else {
                // lock the mutex for writing to the csv on the disk
                if (flock($handle, LOCK_EX)) {
                    $write_result = fwrite($handle, $latest_csv_data);
                    flock($handle, LOCK_UN);

                    if ($write_result === false) {
                        echo "<h2>Error: write to csv file failed</h2>";
                    }
                } else {
                    echo "<h2>Error: couldnt lock csv mutex</h2>";
                }

                fclose($handle);
            }
        }

        fclose($temp_handle);
    } else {
        $msg = "<h2>Error: while parsing xlsl data from url" . $remote_filename_xlsl . "</h2>";
        echo $msg;
        echo SimpleXLSX::parseError();
    }
}